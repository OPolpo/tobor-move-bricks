/**
 * @author OPolpo - Andrea F. Bocchese
 * @date 20 May 2015
 * @brief Cube Detector.
 */

#ifndef CUBE_DET
#define CUBE_DET
int start_detection();
void start_video();

typedef struct point{
  double x;
  double y;
}point;

void * detection_thread(void * t_params);

#else
#endif
