#Semplice makefile per compilare programmi Aria composti da un unico file
#residenti in qualunque directory

PROGRAM_NAME=move_bricks
VERSION=1.0

#ALL_FILES = *.cpp *.h Makefile TODO CHANGELOG COPYING *.map
ALL_FILES = *.cpp Makefile 

OTHERS=cube_detector


ARIA_INCLUDE_PATH=/usr/local/Aria/include
ARNL_INCLUDE_PATH=/usr/local/Arnl/include
ARNL_NET_INCLUDE_PATH=/usr/local/Arnl/include/ArNetworking
ARIA_LINK_PATH=/usr/local/Aria/lib
ARNL_LINK_PATH=/usr/local/Arnl/lib
LIBTARGETS_BASE=/usr/local/Arnl/lib/libBaseArnl.so /usr/local/Arnl/lib/libArNetworkingForArnl.so /usr/local/Arnl/lib/libAriaForArnl.so 

CXX=g++
CXXINC=-Iinclude -I$(ARIA_INCLUDE_PATH) -I$(ARNL_INCLUDE_PATH) -I$(ARNL_NET_INCLUDE_PATH)  -I/usr/include
CXXLINK=-Llib -L$(ARIA_LINK_PATH) -L$(ARNL_LINK_PATH) -lAria -lBaseArnl -lArNetworkingForArnl -lAriaForArnl -lArnl -lpthread -ldl -lrt
CVLIB=`pkg-config opencv cvblob --cflags --libs`

BARECXXFLAGS= -g -Wall -D_REENTRANT
CXXFLAGS+=$(BARECXXFLAGS) -fno-exceptions

all:	$(PROGRAM_NAME)

$(PROGRAM_NAME): $(PROGRAM_NAME).o $(OTHERS).o
	$(CXX) $(CXXFLAGS) $(CXXINC) $(PROGRAM_NAME).o $(OTHERS).o -o $@ $(CXXLINK) $(CVLIB)

$(PROGRAM_NAME).o: $(PROGRAM_NAME).cpp
	$(CXX) -c $(CXXFLAGS) $(CXXINC) $(CVLIB) $< -o $@

$(OTHERS).o: $(OTHERS).cpp
	$(CXX) -c $(CXXFLAGS) $(CXXINC) $(CVLIB) $< -o $@

clean:
	rm -rf *~
	rm -rf *.o
	rm -rf $(PROGRAM_NAME)

install:	all
	cp $(PROGRAM_NAME) /usr/local/bin/$(PROGRAM_NAME)

dist:	clean
	mkdir $(PROGRAM_NAME)
	cp $(ALL_FILES) $(PROGRAM_NAME)
	tar czvf ../$(PROGRAM_NAME)-V$(VERSION).tgz $(PROGRAM_NAME)
	rm -rf $(PROGRAM_NAME)
# All done!


