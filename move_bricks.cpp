/**
 * @author OPolpo - Andrea F. Bocchese
 * @author Andrea Inserra
 * @author Matteo Peroni
 * @date 30 jun 2015
 * @brief Cube Detector.
 */

#include "Aria.h"
#include "ArNetworking.h"
#include "Arnl.h"
#include "ArPathPlanningInterface.h"
#include "ArLocalizationTask.h"
#include "ArDocking.h"
#include "ArSystemStatus.h"
#include "cube_detector.h"
#include <pthread.h>
#include "cube_detector.h"

extern point nearest_cube;

#define PI 3.141592

#define DEBRIS_FIELD "5" // probabilmente non verra' utilizzato
#define BLUE_DUMP    "1"
#define YELLOW_DUMP  "3"
#define GREEN_DUMP   "2"
#define RED_DUMP     "1"

#define FIELD_DUMP_GUARD_DISTANCE 1000 // distanza dal field dump entro la quale cercare ui cubi


int temp = 0;
double amount, score;
bool planToGoalBool;
bool busy = 0;
ArTime start;
ArPose last_pose;
ArPose field_dump_pose;
ArPose goal_pose;



// stato del robot
enum robot_state {
   SEARCHING,
   RENDEZVOUS_HEADING,
   RENDEZVOUS_MOVING,
   RENDEZVOUS_PICKING,
   PLANNING_TO_FIELD,
   PLANNING_TO_LAST,
   FIELD_FAILED,
   LAST_FAILED,
   GOING,
   TO_THE_DUMP,
   DUMPED
   // GOING_TO_DEBRIS
};

robot_state stato;

// ArGlobalFunctor2<ArRobot*, ArPathPlanningTask*> goto_debris_field_CB;
// ArGlobalFunctor3<ArRobot*, ArGripper*, ArPathPlanningTask*> goto_field_dump_CB;
ArGlobalFunctor2<ArRobot*, ArLocalizationTask*> score_CB;
ArGlobalFunctor show_nearest_cube_CB;
ArGlobalFunctor2<ArRobot*, ArGripper*> show_gripper_status_CB;


/**
 * @brief funzione che stampa a schermo stato del robot e score localizzazione
 *
 * @param robot istanza del robot
 * @param locTask localization task
 * 
 * @see print_score_CB()
 *
 * Questa funzione stampa a schermo lo score di localizzazione e lo stato (robot state)
 * del robot. Utilizzata dalla callback print_score_CB
 */
void print_score(ArRobot* robot, ArLocalizationTask* locTask){
  //if (temp++ % 200) return;  
  score = locTask->getMCLocalizationScore();
  printf("\n-- Localization score: %f\n",score);
  printf("-- Stato             : %d\n",stato);
}

/**
 * @brief funzione che stampa a schermo le coordinate del cubo piu vicino
 * 
 * @see show_nearest_cube_CB()
 *
 * Questa funzione stampa a schermo le coordinate del cubo piu vicino
 * (-1,-1) se non rilevato nessun cubo
 */
void show_nearest_cube(){
  //if (temp++ % 10) return;  
  printf("\n-- Nearest CUBE: %f,%f\n",nearest_cube.x,nearest_cube.y);
}

/**
 * @brief routine per scaricare il cubo.
 *
 * @param robot istanza del robot
 * @param gripper pinza del robo
 * 
 *
 * Questa funzione serve per eseguire la procedura di deployment del cubo
 * il robot apre le pinze indietreggia e si gira di 180 gradi.
 */
void deploy_the_cube(ArRobot* robot, ArGripper* gripper){
  printf("\nDEPLOY TASK");
  robot->clearDirectMotion();

  robot->lock();
  robot->clearDirectMotion();
  gripper->gripOpen();
  ArUtil::sleep(1000);
  stato = DUMPED;
  robot->setAbsoluteMaxTransVel(400);
  robot->move(-500);


  robot->unlock();
  ArUtil::sleep(2000);
  robot->lock();
  robot->clearDirectMotion();
  robot->setAbsoluteMaxRotVel(100.0);
  robot->setDeltaHeading(-180);
  robot->unlock();
  ArUtil::sleep(2000);

  stato = PLANNING_TO_LAST;
}

/**
 * @brief funzione per la pianificazione del percorso
 *
 * @param robot istanza del robot
 * @param pose posa da raggiungere
 * @param patkTask path task
 *
 * Questa funzione serve per lanciare la pianificazione del percorso verso
 * la posa data.
 */
void planning(ArRobot* robot, ArPose pose, ArPathPlanningTask* pathTask){
  robot->clearDirectMotion();
  goal_pose = pose;
  pathTask->pathPlanToPose(goal_pose,true);
  stato = GOING;
}

/**
 * @brief funzione per il recovery in caso di fault di painificazione
 *
 * @param robot istanza del robot
 * @param constantVelocity azione da aggiungere (velocita' costante)
 * 
 * questa funzione serve per cercare di innalzare lo score di localizzazione 
 * al fine di riuscire a pianificare il percorso verso la posa fallita.
 */
void failed_routine(ArRobot* robot, ArActionConstantVelocity * constantVelocity){
  if(score < 0.8){
    robot->lock();
    // robot->clearDirectMotion();
    robot->setAbsoluteMaxRotVel(50.0);
    robot->setDeltaHeading(360);
    robot->unlock();
    ArUtil::sleep(4000);
    if(robot->findAction("Constant Velocity") == NULL){
      printf("\nriaggiungo la velocita");
      robot->addAction(constantVelocity, 35);
    }
  }
}

/**
 * @brief funzione per determinare se il cubo e' da pendere.
 *
 * @param robot istanza del robot
 * @param field_dump_pose posa del deposito
 * @param x cooridnata x cubo rispetto a robot (C2)
 * @param y coordinata y cubo rispetto a robot (C2)
 * 
 * Questa funzione serve per determinare se il cubo e' sufficientemente distante
 * dal deposito. In caso il ritorna vero altrimenti Falso.
 */
bool suitable_cube(ArRobot* robot, ArPose field_dump_pose, double x, double y){
  ArPose robot_pose = robot->getPose();
  double distance_cube_robot = sqrt(pow(x,2)+pow(y,2)) * 10;
  ArPose cube_pose(robot_pose.getX() + distance_cube_robot*sin(robot->getTh()), robot_pose.getY() + distance_cube_robot*cos(robot->getTh())); // da verificare
  printf("\n\nIL CUBO DISTA %f CM dalla BASE\n\n",cube_pose.findDistanceTo(field_dump_pose));
  return cube_pose.findDistanceTo(field_dump_pose) > FIELD_DUMP_GUARD_DISTANCE;
}

/**
 * @brief routine di orientamento
 *
 * @param robot istanza del robot.
 * @param gripepr pinza del robot.
 * @param field_dump_pose posa del deposito.
 *
 * Questa funzione serve per orientare il robot in direzione del cubo piu' vicino.
 */
void cube_rendezvous_heading(ArRobot* robot, ArGripper* gripper, ArPose field_dump_pose){

  double cube_x = nearest_cube.x;
  double cube_y = nearest_cube.y;
  
  if(cube_y !=-1 && suitable_cube(robot, field_dump_pose, cube_x, cube_y)){
    robot->clearDirectMotion();
    printf("\nHEADING TASK");
    if(gripper->getGripState() != 1)
      gripper->gripOpen();

    double angle = - atan(cube_x / cube_y)*180/PI;
  
    printf("\n\nangle %f", angle);
    printf("\nY: %f", cube_y);
    printf("\nX: %f", cube_x);
    
    start.setToNow();
    robot->lock();
    robot->setAbsoluteMaxRotVel(20.0);
    robot->setDeltaHeading(angle);

    robot->unlock();  
    ArUtil::sleep(3000);  
    stato = RENDEZVOUS_MOVING;
  }
  else
    stato = SEARCHING;
}

/**
 * @brief routine di avvicinamento
 *
 * @param robot istanza del robot.
 * @param field_dump_pose posa del deposito.
 *
 * Questa funzione serve per muovere il robot in direzione del cubo frontale (se il piu vicino).
 */
void cube_rendezvous_moving(ArRobot* robot, ArPose field_dump_pose){
  //printf("\nMi sono girato di una volta\n");
  // ArUtil::sleep(5000);  
  double cube_x = nearest_cube.x;
  double cube_y = nearest_cube.y;

  if(cube_y !=-1 && suitable_cube(robot, field_dump_pose, cube_x, cube_y)){
    robot->clearDirectMotion();
    printf("\nMOVING TASK");
    
    double angle = - atan(cube_x / cube_y)*180/PI;
    printf("\n\nangle %f", angle);
    printf("\nY: %f", cube_y);
    printf("\nX: %f", cube_x);
    
    if(abs(angle) > 3){
      stato = RENDEZVOUS_HEADING;
      return;
    }
    
    double distance = sqrt(pow(nearest_cube.y, 2)+pow(nearest_cube.x, 2));

    //ArUtil::sleep(500);
    double speed = 400.0;
    // robot->clearDirectMotion();
    
    printf("\navanzo una volta di %f\n", distance);
    robot->lock();      
    robot->setAbsoluteMaxTransVel(speed);
    robot->move(distance*10 + 25);
    robot->unlock();
    start.setToNow();

    ArUtil::sleep(1000);  
    stato = RENDEZVOUS_PICKING;
  }
  else
    stato = SEARCHING;
}

/**
 * @brief routine di sollevamento del cubo
 *
 * @param robot istanza del robot.
 * @param gripepr pinza del robot.
 * @param field_dump_pose posa del deposito.
 *
 * Questa funzione serve per afferrare il cubo che si trva tra le pinze (se presente).
 */
void cube_rendezvous_picking(ArRobot* robot, ArGripper* gripper, ArPose field_dump_pose){
    printf("\nPICKING TASK");
    start.setToNow();

    robot->lock();
    if(gripper->getBreakBeamState() != 0){
      robot->clearDirectMotion();
      gripper->gripClose();
      stato = PLANNING_TO_FIELD;
      last_pose = robot->getPose();
    }

    // ho mancato il cubo
    else{
      stato = SEARCHING;
      printf("\nHo mancato il cubo\n");
    }
    robot->unlock();
    ArUtil::sleep(2000);
}

/**
 * @brief funzione che stampa a schermo lo stato della pinza
 * 
 * @see show_gripper_status_CB()
 *
 * Questa funzione stampa a schermo lo stato della pinza e dei sensori presenti
 * su di essa.
 */
void show_gripper_status(ArRobot* robot, ArGripper* gripper){
  robot->lock();
  std::string beam_status;
  std::string grip_status;
  switch(gripper->getBreakBeamState()){
    case 0: beam_status = "ALL beams CLEAR"; break;
    case 1: beam_status = "INNER beam INTERRUPTED"; break;
    case 2: beam_status = "OUTER beam INTERRUPTED"; break;
    case 3: beam_status = "ALL beams INTERRUPTED"; break;
    default:printf("BEAM - FAILURE\n");
    abort();
  }
  switch (gripper->getGripState()) {
    case 0: grip_status = "BETWEEN"; break;
    case 1: grip_status = "OPEN"; break;
    case 2: grip_status = "CLOSED"; break;
    case 3: grip_status = "MIDDLE"; break;
    default:printf("STATE - FAILURE\n");
    abort();
  }

  printf("\nGripper is %s with %s", grip_status.c_str(), beam_status.c_str());
  robot->unlock();
}

/**
 * @brief callback caso successo pianificazione
 * 
 * @param posa raggiunta
 *
 * Call back in chiamata in caso di raggiunta della posa
 */
void goal_done(ArPose pose){
  // printf("\n## Goal DONE");
  if(pose == field_dump_pose){
    printf("\nArrivato a FIELD DUMP\n");
    stato = TO_THE_DUMP;
  }
  else{
    printf("\nArrivato a LAST\n");
    stato = SEARCHING;
  }
}

/**
 * @brief callback caso fallimento pianificazione
 * 
 * @param posa fallita
 *
 * Call back in chiamata in caso di fallimnento nel raggiungere la posa
 */
void goal_failed(ArPose pose){
  if(pose == field_dump_pose){
    printf("\nFALLITO FIELD DUMP\n");
    stato = FIELD_FAILED;
  }
  else{
    printf("\nFallito LAST\n");
    printf("\n## Goal Failed\n");
    stato = LAST_FAILED;
  } 
}

/**
 * @brief callback caso interruzione pianificazione
 * 
 * @param posa interrotta
 *
 * Call back in chiamata in caso di interruzion della pianificazione verso posa
 */
void goal_interrupt(ArPose pose){
  printf("\n## Goal Interrupted\n");
}


ArGlobalFunctor1<ArPose> goal_done_CB;
ArGlobalFunctor1<ArPose> goal_failed_CB;
ArGlobalFunctor1<ArPose> goal_interrupt_CB;
  

/**
 * @brief logica di controllo del robot
 * 
 *
 * Programma principale, contiene le fasi di connessione al robot,
 * istanziazione delle strutture di controllo ed avvio dei task di localizzazione
 * e pianificazione.
 */
int main(int argc, char *argv[]){
  Aria::init();
  Arnl::init();
  
  ArRobot robot;
  ArGripper gripper(&robot);

  ArArgumentParser parser(&argc, argv);
  parser.addDefaultArgument("-connectLaser");
  parser.loadDefaultArguments();

  // Check for --help
  if (!parser.checkHelpAndWarnUnparsed()){
    printf("\nUsage: simpleDemo [-map <map file>] [other options]\n\nIf -map not given, use map from ARNL configuration file (%s).\n\n", Arnl::getTypicalParamFileName());
    Aria::exit(4);
  }


  ArRobotConnector robotConnector(&parser, &robot);

  // Connect to the robot
  if (!robotConnector.connectRobot()){
    printf("Error: Could not connect to robot... exiting");
    Aria::exit(3);
  }

  ArSonarDevice sonarDev;
  robot.addRangeDevice(&sonarDev);

  ArLaserConnector laserConnector(&parser, &robot, &robotConnector);

  // connect the laser(s) if it was requested, this adds them to the
  // robot too, and starts them running in their own threads
  printf("Connecting to laser(s) configured in parameters...");
  if (!laserConnector.connectLasers()){
    printf("Error: Could not connect to laser(s). Exiting.");
    Aria::exit(2);
  }
  printf("Done connecting to laser(s).");

  robot.lock();
  ArLaser *firstLaser = robot.findLaser(1);
  if (firstLaser == NULL || !firstLaser->isConnected()){
    ArLog::log(ArLog::Normal, "Did not have laser 1 or it is not connected, cannot start localization and/or mapping... exiting");
    Aria::exit(2);
  }
  robot.unlock();

  ArMap arMap;

  ArLocalizationTask locTask(&robot, firstLaser, &arMap);
  ArPathPlanningTask pathTask(&robot, firstLaser, &sonarDev, &arMap);

  ArPoseStorage poseStorage(&robot);
  if (poseStorage.restorePose("robotPose"))

  
  // Read ARNL parameter file
  Aria::getConfig()->useArgumentParser(&parser);
  if (Aria::getConfig()->parseFile(Arnl::getTypicalParamFileName())){
    printf("Loaded configuration file %s\n", Arnl::getTypicalParamFileName());
  }
  else{
    printf("Trouble loading configuration file, exiting\n");
    Aria::exit(5);
  }

  // Add the laser to the robot
  robot.addRangeDevice(firstLaser);

  // Start the robot thread.
  robot.runAsync(true);

  ArUtil::sleep(300);

  // Localize the robot to home
  if(locTask.localizeRobotAtHomeBlocking()){
    printf("Successfully localized at home.\n");
  } 
  else{
    printf("Error: Unable to localize in map (based on home position)!\n");
    Aria::exit(3);
  }

  // Enable motors and Wait for program cancelled or robot connection lost
  robot.lock();
  robot.enableMotors();
  robot.unlock();


  ArMapObject* field_dump = arMap.findMapObject(BLUE_DUMP);
  field_dump_pose = field_dump->getPose();

  ArKeyHandler *keyHandler;
  if ((keyHandler = Aria::getKeyHandler()) == NULL){
    keyHandler = new ArKeyHandler;
    Aria::setKeyHandler(keyHandler);
    robot.lock();
    robot.attachKeyHandler(keyHandler);
    robot.unlock();
  }

  show_gripper_status_CB = ArGlobalFunctor2<ArRobot*, ArGripper*>(&show_gripper_status, &robot, &gripper);
  keyHandler->addKeyHandler('g', &show_gripper_status_CB);

  score_CB = ArGlobalFunctor2<ArRobot*, ArLocalizationTask*>(&print_score, &robot, &locTask);
  keyHandler->addKeyHandler('s', &score_CB);


  show_nearest_cube_CB = ArGlobalFunctor (&show_nearest_cube);
  keyHandler->addKeyHandler('n', &show_nearest_cube_CB);


  goal_done_CB = ArGlobalFunctor1<ArPose> (&goal_done, goal_pose);
  goal_failed_CB = ArGlobalFunctor1<ArPose> (&goal_failed, goal_pose);
  goal_interrupt_CB = ArGlobalFunctor1<ArPose> (&goal_interrupt, goal_pose);

  pathTask.addGoalDoneCB(&goal_done_CB); // Implement each as task callbacks
  pathTask.addGoalFailedCB(&goal_failed_CB);
  pathTask.addGoalInterruptedCB(&goal_interrupt_CB);
  
  //se la pinza non e' aperta la apro
  if(gripper.getGripState()!=1)
    gripper.gripOpen();

  //Avvio il cube detector
  pthread_t t;
  pthread_create(&t, NULL, &detection_thread, NULL);

  ArUtil::sleep(4000);

  ArActionStallRecover recover;
  ArActionBumpers bumpers;
  ArActionAvoidFront avoidFrontNear("Avoid Front Near", 225, 0);
  ArActionAvoidFront avoidFrontFar;
  ArActionConstantVelocity constantVelocity("Constant Velocity", 50);


  robot.addAction(&recover, 100);
  robot.addAction(&bumpers, 75);
  robot.addAction(&avoidFrontNear, 50);
  //robot.addAction(&avoidFrontFar, 49);

  stato = SEARCHING;
  // stato = PLANNING;
  // stato = GOING;
  // robot.setAbsoluteMaxTransVel(300.0);

  // if(score < 0.6){
  //   robot.setAbsoluteMaxRotVel(50.0);
  //   robot.setDeltaHeading(360);
  // }

  while(1){
    // printf("\n%d\n",stato);
    // if(stato != SEARCHING)
    //   robot.remAction(&constantVelocity);
    ArUtil::sleep(500);

    switch(stato){
      case SEARCHING:{
        robot.clearDirectMotion();
        double cube_x = nearest_cube.x;
        double cube_y = nearest_cube.y;
        if(cube_y !=-1 && suitable_cube(&robot, field_dump_pose, cube_x, cube_y)){
          stato = RENDEZVOUS_HEADING;
          if(robot.findAction("Constant Velocity") != NULL){
            printf("\nRIMUOVO VELOCITA");
            robot.remAction(&constantVelocity);
          }
          break;
        }
        // ArActionConstantVelocity constantVelocity("Constant Velocity", 200);
        if(robot.findAction("Constant Velocity") == NULL){
          printf("\nriaggiungo la velocita");
          robot.addAction(&constantVelocity, 35);
        }
        break;
      }
      case RENDEZVOUS_HEADING:{
        cube_rendezvous_heading(&robot, &gripper, field_dump_pose);
        break;
      }
      case RENDEZVOUS_MOVING:{
        cube_rendezvous_moving(&robot, field_dump_pose);
        break;
      }
      case RENDEZVOUS_PICKING:{
        cube_rendezvous_picking(&robot, &gripper, field_dump_pose);
        break;
      }
      case PLANNING_TO_FIELD:{
        printf("\nPLANNING TO FIELD DUMP");
        planning(&robot, field_dump_pose, &pathTask);
        break;
      }
      case PLANNING_TO_LAST:{
        printf("\nPLANNING TO LAST\n");
        planning(&robot, last_pose, &pathTask);
        break;
      }
      case GOING:{
        printf(".");
        break;
      }
      case FIELD_FAILED:{
        failed_routine(&robot, &constantVelocity);
        stato = PLANNING_TO_FIELD;
        break;
      }
      case LAST_FAILED:{
        failed_routine(&robot, &constantVelocity);
        stato = PLANNING_TO_LAST;
        break;
      }
      case TO_THE_DUMP:{
        deploy_the_cube(&robot, &gripper);
        break;
      }
      case DUMPED:{
        stato = SEARCHING;
        break;
      }
    }
  }
  //robot.addAction(&constantVelocity, 25);

  robot.waitForRunExit();
  exit(0);

}
