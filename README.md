Tobor - Move Bricks

Tobor is a MobileRobots Pioneer 1 robot equipped with a tong. The project consists in the localization of small colored bricks using the robot camera. After the localization, the robot picks up the brick and moves it in a given position.
