/**
 * @author OPolpo - Andrea F. Bocchese
 * @date 20 May 2015
 * @brief Cube Detector.
 * 
 * Programma per il detect di cubi colorati, tale programma e' tarato per il 
 * funzionamento su una camera di dimensioni note e con orientamento
 * ben preciso.
 * Tale programma screive sulla variabile nearest_cube le coordinate del cubo
 * piu vicino rilevato
 * 
 */

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <math.h>
#include <pthread.h>


#if (defined(_WIN32) || defined(__WIN32__) || defined(__TOS_WIN__) || defined(__WINDOWS__) || (defined(__APPLE__) & defined(__MACH__)))
#include <cv.h>
#include <highgui.h>
#else
#include <opencv/cv.h>
#include <opencv/highgui.h>
#endif

#include <cvblob.h>
#include "cube_detector.h"
using namespace cvb;
using namespace std;
using namespace cv;


#define MAX_DISTANCE 210
#define FURTER_HORIZONTAL_LEN 210
#define NEAREST_HORIZONTAL_LEN 48
#define TONG_TO_LAST_VISIBLE 10

int camera_v = 480;
int camera_h = 640;

struct params {
  CvCapture *capture;
};

Mat mask;

point nearest_cube = {-1,-1};

//this function convert a given point from the screen coordinates to the real space coordinates
//the 0,0 in real world corespond to an object exactly in the centre of the tong

/**
 * @brief funzione per la conversione da coordinate del bloob sull'immagine a
 *        coordinate rispetto al robot (cm).
 *
 * @param screen_x coordinata x del blob.
 * @param screen_y coordinata y del blob.
 * @param real punto reale (output).
 * 
 * Questa funzione converte le coordinate nel sistema di riferimento dell'immagine
 * Coordinate dei pixel in coordinate spaziali del sistema C2 (vedi relazione annessa)
 */
point* screen_to_real_point(int screen_x, int screen_y, point* real){
  real->y = 0.8*(camera_v-screen_y)*pow(MAX_DISTANCE,2)/pow(camera_v,2) + TONG_TO_LAST_VISIBLE;
  real->x = ((screen_x-camera_h*0.5)/(camera_h*0.5))*((((real->y - TONG_TO_LAST_VISIBLE)/MAX_DISTANCE)*((FURTER_HORIZONTAL_LEN - NEAREST_HORIZONTAL_LEN) * 0.5)) + NEAREST_HORIZONTAL_LEN*0.5);
  return real;
}

/**
 * @brief funzione che ritorna la label del cubo piu vicino.
 *
 * @param blobs blobs rilevati
 * 
 * Questa funzione riturna la label del blob piu grande rilevato coerentemente 
 * con la sua posizione nell'immagine. Tale funzione non usa la distanza dal
 * robot ma semlicemente la componente y del blob sull'immagine.
 */
int cvMyLargestBlob(const CvBlobs &blobs){
  CvLabel label=-1;
  unsigned int maxArea=0;

  for (CvBlobs::const_iterator it=blobs.begin();it!=blobs.end();++it){
    CvBlob *blob=(*it).second;
    if (blob->area < 8500-25*(camera_v-blob->centroid.y) && blob->area > maxArea){
      label=blob->label;
      maxArea=blob->area;
    }
  }

  return (int)label;
}

/**
 * @brief Creazione della maschera. 
 *
 * @param cols colonne frame (della variabile globale).
 * @param rows righe frame (della variabile globale).
 * 
 * Questa funzione definisce la maschera fa uso di variabile globale mask.
 *
 */
void build_mask(int rows, int cols){
  mask = Mat::zeros(Size(cols,rows), CV_8UC1);
  mask(Range(mask.rows*3/10,mask.rows),Range(0,mask.cols))=255;
  
  cv::Point polygonSx[] = {cv::Point(mask.cols/4,mask.rows*3/10),cv::Point(0,mask.rows*3/10),cv::Point(0,mask.rows)};
  fillConvexPoly(mask, polygonSx, 3, Scalar(0));
  cv::Point polygonDx[] = {cv::Point(mask.cols-mask.cols/4,mask.rows*3/10),cv::Point(mask.cols,mask.rows*3/10),cv::Point(mask.cols,mask.rows)}; 
  fillConvexPoly(mask, polygonDx, 3, Scalar(0));
}


/**
 * @brief thread principale di rilevamento del cubo
 *
 * 
 * Questa funzione accede alla camera ed ad ogni frame applica le operazioni
 * necessarie per rilevare il cubo piu vicino.
 */
void* detection_thread(void* t_params)
{
  CvCapture *capture = cvCaptureFromCAM(0);

  cvGrabFrame(capture);
  IplImage *img = cvRetrieveFrame(capture);

  //IplImage *img = cvLoadImage("_orig.jpg",1);

  build_mask(img->height,img->width);
  Mat mat_image(img);
  Mat masked_image;
  mat_image.copyTo(masked_image,mask);
  img = cvCreateImage(Size(masked_image.cols,masked_image.rows), 8, 3 );
  img->imageData=(char*)masked_image.data;

  CvSize imgSize = cvGetSize(img);
  camera_v = imgSize.height;
  camera_h = imgSize.width;

  IplImage *frame = cvCreateImage(imgSize, img->depth, img->nChannels);

  IplConvKernel* morphKernel = cvCreateStructuringElementEx(5, 5, 1, 1, CV_SHAPE_RECT, NULL);

  //unsigned int frameNumber = 0;
  //unsigned int blobNumber = 0;

  while (cvGrabFrame(capture)){

    IplImage *img = cvRetrieveFrame(capture);
    Mat mat_frame(img);
    Mat masked;
    mat_frame.copyTo(masked,mask);
    //imwrite("img.jpg",masked);
    img = cvCreateImage(Size(masked_image.cols,masked_image.rows), 8, 3 );
    img->imageData=(char*)masked.data;

    cvConvertScale(img, frame, 1, 0);

    IplImage *thresholded = cvCreateImage(imgSize, 8, 1);

//    double H = 0.5;
//    double L = 0.3;

    double d1=0;
    double d2=0;
    double d3=0;
    int i,j;
    for (j=0; j<imgSize.height; j++)
      for (i=0; i<imgSize.width; i++)
      {
        CvScalar c = cvGet2D(frame, j, i);

        double b = ((double)c.val[0]*1.0);
        double g = ((double)c.val[1]*1.0);
        double r = ((double)c.val[2]*0.7); //spengo leggermente i toni del rosso
        

        d1=pow(r-g,2)/65025.;
        d2=pow(r-b,2)/65025.;
        d3=pow(g-b,2)/65025.;
        unsigned char f = 255*((abs(d1)+abs(d2)+abs(d3)) > .1 + .20*(r+g+b)/765);
        cvSet2D(thresholded, j, i, CV_RGB(f, f, f));
      }

    cvMorphologyEx(thresholded, thresholded, NULL, morphKernel, CV_MOP_OPEN, 1);
    // imwrite("thresholded.jpg",thresholded);

    IplImage *labelImg = cvCreateImage(cvGetSize(frame), IPL_DEPTH_LABEL, 1);

    CvBlobs blobs;
    // unsigned int result = cvLabel(thresholded, labelImg, blobs);
    cvLabel(thresholded, labelImg, blobs);
    cvFilterByArea(blobs, 500, 1000000);

    CvBlob* pointer;
    CvLabel llabel = cvMyLargestBlob(blobs);
    if((int)llabel == -1){
      nearest_cube.x = -1;
      nearest_cube.y = -1;
    }
    else{
      for (CvBlobs::const_iterator it=blobs.begin(); it!=blobs.end(); ++it)
      {
        pointer = it->second;
        if(pointer->label==llabel){
          screen_to_real_point(pointer->centroid.x, pointer->centroid.y, &nearest_cube);
          //cout << pointer->centroid.x << "," << pointer->centroid.y << " ---> " << nearest_cube.x << "," << nearest_cube.y << endl;
          //cout << pointer->area<<endl; 
          cvCircle(frame,cvPoint(pointer->centroid.x,pointer->centroid.y),10,CV_RGB(0,255,0),-1);
        }
      }
    }
    // imwrite("detected.jpg",frame);

    cvReleaseImage(&labelImg);
    cvReleaseImage(&thresholded);

    // cvWaitKey(10)&0xff;
    cvWaitKey(10);

    cvReleaseBlobs(blobs);

  }

  cvReleaseStructuringElement(&morphKernel);
  cvReleaseImage(&frame);
  return NULL;
}
